﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cdk.Dashboard.Model;
using Cdk.Dashboard.DataRepository;
using Moq;
using Cdk.Core.Common;
using System.Collections.Generic;
using Cdk.Dashboard.IndexManager;
using Cdk.Dashboard.Parsers.Model_Parsers;
namespace Cdk.DashBoardEngine
{
    /// <summary>
    /// Summary description for OrderRepositoryTest
    /// </summary>
    [TestClass]
    public class OrderRepositoryTest
    {

        
        [TestMethod]
        public void GetIdFromOrderShouldReturnNull()
        {
            //Mock Index Manager
            var _indexManager = new Mock<IIndexManager>();
            _indexManager.Setup(m => m.GetIndexValue(It.IsAny<int>(), It.IsAny<IndexType>()))
                .Returns(new Func<ICollection<long>>(() =>
                {
                    List<long> lisL = new List<long> { 1 };
                    return (lisL);
                }

                    ));
            //Mock Parser
            var _Parser = new Mock<IModelParser<OrderModel>>();
            _Parser.Setup(m => m.ParseIntoModelFromFile(null)).Returns(new Func<List<OrderModel>>(() =>
            {
                var model = new List<OrderModel>();
                model.Add(new OrderModel() { ColorCar = "Test" });
                return (model);
            }));
            _Parser.Setup(m => m.ParseIntoModel("")).Returns(new OrderModel() { ColorCar = "Test" });

            //Mock Repository Manager
            var _mockRepositoryManager = new Mock<IRepositoryManager>();
            _mockRepositoryManager.Setup(m => m.GetIndexer()).Returns(_indexManager.Object);

            //Mock Order REpository
            var _orderRepository = new Mock<OrderRepository<OrderModel>>(_mockRepositoryManager.Object, _Parser.Object);
            var result = _orderRepository.Object.GetById(1);
            //Record not present 
            Assert.IsNull(result);
        }

      
    }
}
