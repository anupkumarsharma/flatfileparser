﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cdk.Dashboard.Model;
using Cdk.Dashboard.DataRepository;
using Moq;
using Cdk.Core.Common;
using System.Collections.Generic;
using Cdk.Dashboard.IndexManager;
using Cdk.Dashboard.Parsers.Model_Parsers;

namespace Cdk.DashBoardEngine
{
    [TestClass]
    public class CustomerRepositoryTest
    {
        Mock<DashboardIndexManager> _dataManager;
        Mock<IModelParser<CustomerModel>> _Parser;
        Mock<CustomerRepository<CustomerModel>> _customerRepository;
        [TestInitialize]
        void InitializeTest()
        {
            
        }

        [TestMethod]
        public void InitializeCustomerRepository()
        {
            
            var _mockRepositoryManager = new Mock<IRepositoryManager>();
            var _Parser = new Mock<IModelParser<CustomerModel>>();

            var _customerRepository = new Mock<CustomerRepository<CustomerModel>>(_mockRepositoryManager.Object, _Parser.Object);
            Assert.IsNotNull(_customerRepository);


        }

        [TestMethod]
        public void GetIdShouldReturn()
        {
          
            var  _indexManager = new Mock<IIndexManager>();
            _indexManager.Setup(m=>m.GetIndexValue(It.IsAny<int>(),It.IsAny<IndexType>()))
                .Returns(new Func<ICollection<long>> (()=>
                       {
                List<long> lisL = new List<long> { 1 };
                return (lisL);
            }
                    
                    ));

            var _Parser = new Mock<IModelParser<CustomerModel>>();
            _Parser.Setup(m => m.ParseIntoModelFromFile(null)).Returns(new Func<List<CustomerModel>>(() =>
                {
                    var model = new List<CustomerModel>();
                    model.Add(new CustomerModel() { FirstName = "Test" });
                    return (model);
                }));
            _Parser.Setup(m => m.ParseIntoModel("")).Returns(new CustomerModel() { FirstName = "Test" });

            var _mockRepositoryManager = new Mock<IRepositoryManager>();
            _mockRepositoryManager.Setup(m => m.GetIndexer()).Returns(_indexManager.Object);



            var _customerRepository = new Mock<CustomerRepository<CustomerModel>>(_mockRepositoryManager.Object, _Parser.Object);
            


          var result=  _customerRepository.Object.GetById(1);
            //Record not present 
          Assert.IsNull(result);

          


        }
    }
}
