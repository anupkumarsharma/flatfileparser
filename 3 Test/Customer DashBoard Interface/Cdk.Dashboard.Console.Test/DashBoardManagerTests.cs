﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cdk.Dashboard.Manger;

namespace Cdk.Dashboard.Console.Test
{
    [TestClass]
    public class DashBoardManagerTests
    {
        [TestMethod]
        public void DashBoardShuoldReturnNameForAGivenCustomerId()
        {
          var  dashboardManager = FactoryDashboard.GetDashBoardObject(true);
          var name = dashboardManager.GetcustomerName(1010);
          Assert.AreEqual(name, "Cusomter1010");

        }

        [TestMethod]
        public void DashBoardShuoldReturnOrderForAGivenCustomerId()
        {
            var dashboardManager = FactoryDashboard.GetDashBoardObject(true);
            var orderlist = dashboardManager.GetOrdersForCustomer(1010);

            foreach (var item in orderlist)
            {
                Assert.AreEqual(item.CustomerId, 1010);
                
            }
            

        }


        [TestMethod]
        public void DashBoardShuoldReturnOrderForAGivenCustomerIdForIndexOff()
        {
            var dashboardManager = FactoryDashboard.GetDashBoardObject(false);
            var orderlist = dashboardManager.GetOrdersForCustomer(1010);

            foreach (var item in orderlist)
            {
                Assert.AreEqual(item.CustomerId, 1010);

            }


        }

        [TestMethod]
        public void DashBoardShuoldReturnNameForAGivenCustomerIdforIndexOff()
        {
            var dashboardManager = FactoryDashboard.GetDashBoardObject(false);
            var name = dashboardManager.GetcustomerName(1010);
            Assert.AreEqual(name, "Cusomter1010");

        }


        [TestMethod]
        [ExpectedException(typeof( CustomerNotFoundException))]
        public void DashBoardShuoldReturnErrorForAGivenCustomerIdforIndexOff()
        {
            var dashboardManager = FactoryDashboard.GetDashBoardObject(false);
            
            var name = dashboardManager.GetcustomerName(1);
            


        }

        [TestMethod]
        public void DashBoardShuoldReturnEmpltyOrderForAGivenCustomerIdForIndexOff()
        {
            var dashboardManager = FactoryDashboard.GetDashBoardObject(false);
            var orderlist = dashboardManager.GetOrdersForCustomer(1);
            Assert.IsNull(orderlist);
        }
    }
}
