﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cdk.Core.FileManager;
using System.IO;

namespace Cdk.Core.Test
{
    [TestClass]
    public class FileManagerRATests
    {
        private const string FIRST_LINE = "ThisisTest";
        private const string TESTFileName = "Testfile.txt";

        
        [TestMethod]
        public void InitializeShouldBeAbleToCreateFileStream()
        {
            FileManagerRa _fileManager = new FileManagerRa(TESTFileName);
            _fileManager.Initialize();

            Assert.IsNotNull(_fileManager.GetFileStream());
        }


        [TestMethod]
        public void InitializeShouldBeAbleToCreateStreamReader()
        {
            FileManagerRa _fileManager = new FileManagerRa(TESTFileName);
            _fileManager.Initialize();

            Assert.IsNotNull(_fileManager.GetStreamReader());
        }

        [TestMethod]
        public void shouldBeAbleToReturnFileStream()
        {
            FileManagerRa _fileManager = new FileManagerRa(TESTFileName);
            _fileManager.Initialize();
            var filStream = _fileManager.GetFileStream();
            Assert.IsNotNull(filStream as FileStream);
        
        }

        [TestMethod]
        public void shouldBeAbleToReturnStreamReader()
        {
            FileManagerRa _fileManager = new FileManagerRa(TESTFileName);
            _fileManager.Initialize();
            var ReaderStream = _fileManager.GetStreamReader();
            Assert.IsNotNull(ReaderStream as StreamReader);

        }
        [TestMethod]
        public void GetLineShouldOutputFirstline()
        {
            FileManagerRa _fileManager = new FileManagerRa(TESTFileName);
            _fileManager.Initialize();
            var firstLine = _fileManager.GetLine(0, true);
            Assert.AreEqual(firstLine, FIRST_LINE);
        
        }

        [TestMethod]
        public void ResetStreamShouldReturnSameLine()
        {
            FileManagerRa _fileManager = new FileManagerRa(TESTFileName);
            _fileManager.Initialize();
            var firstLine = _fileManager.GetLine(0, true);
            Assert.AreEqual(firstLine.ToString(), FIRST_LINE.ToString());
            _fileManager.ResetStream();
            var sameLine = _fileManager.GetLine(0, true);
            if (sameLine.Contains(FIRST_LINE))
            { Assert.IsTrue(true); }
            
            else
	{
                Assert.Fail();

	}
        }

        [TestMethod]
        public void ReadLineAndGetPositionShouldIdentifyLocation()
        {

            FileManagerRa _fileManager = new FileManagerRa(TESTFileName);
            _fileManager.Initialize();
            string firstLine;
            var position = _fileManager.ReadLineAndGetPosition(out firstLine);
            Assert.AreEqual(position, 0);
            Assert.AreEqual(firstLine, FIRST_LINE);
        
        }
    }
}
