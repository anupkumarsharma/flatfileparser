﻿using System;
using System.IO;
using Cdk.Core.Common.Interface;

namespace Cdk.Core.FileManager
{
    /// <summary>
    /// This class provides Random or Index based access to the file and
    ///  encapsulates any operation performed on file
    /// </summary>
    public class FileManagerRa : IFileManager
    {
        private readonly string _fileName;
        private FileStream _stream;
        private StreamReader _streamReader;
        public FileManagerRa(string fileName)
        {
            _fileName = fileName;
        }

        /// <summary>
        /// Manages single instance to a file with below specification
        ///  Mode = Read Only
        ///  Access = Random access 
        /// </summary>
        /// <returns></returns>
        public void Initialize(int bufferSize=1000 )
        {
            if (_stream != null)
            {
                _stream.Close();
            }
            _stream = new FileStream(_fileName,
                FileMode.Open, 
                FileAccess.Read,
                FileShare.Read,
                bufferSize,
                FileOptions.RandomAccess);
            _streamReader = new StreamReader(_stream);

            

        }
        /// <summary>
        /// Returns the underlying the FileStream
        /// </summary>
        /// <returns>FileStream Instance</returns>
        public FileStream GetFileStream()
        { // define expcetion type it can throw

            if (_stream == null)
            {
                throw new TypeInitializationException("FileStream", new Exception() { });

            }
            return (_stream);

        }
        /// <summary>
        /// Return the underlying the StreamReader
        /// </summary>
        /// <returns>StreamReader instance</returns>
        public StreamReader GetStreamReader()
        {
            if (_stream != null)
                _streamReader = new StreamReader(_stream);
            return (_streamReader);

        }

        /// <summary>
        /// This method reads the line from position specified
        /// </summary>
        /// <param name="position">New buffer position</param>
        /// <param name="resetBufferPostion">Buffer Reset?</param>
        /// <returns>string of line</returns>
        public string GetLine(long position, bool resetBufferPostion)
        {
            if (resetBufferPostion)
            {
                _stream.Position = 0;
                _streamReader.DiscardBufferedData();
            
            }
            if (position != 0)
            {
                _stream.Seek(position, SeekOrigin.Begin);
            }

            return(_streamReader.ReadLine());
        
        }

        /// <summary>
        /// Dispose all the handles of stream
        /// </summary>
        public void Dispose()
        {
            if (_streamReader != null)
            {
                _streamReader.Close();
                _streamReader.Dispose();

            }

            if (_stream != null)
            {
                _stream.Close();
                _stream.Dispose();

            }
        }
        /// <summary>
        /// Reset the stream to beginning of the file
        /// </summary>
        public void ResetStream()
        {
            _stream.Position = 0;
            _streamReader.DiscardBufferedData();
        }
        /// <summary>
        /// Returns the position and line read 
        /// </summary>
        /// <param name="lineoftext">line of string</param>
        /// <returns>position of buffer prior to reading the line</returns>

        public long ReadLineAndGetPosition(out string lineoftext)
        {
            var initialPosition = _streamReader.GetPosition();
            if (_streamReader.EndOfStream)
            {
                lineoftext = String.Empty;
                return (-1);
            }
            lineoftext = _streamReader.ReadLine();

            return (initialPosition);
        }
    }
}
