﻿using System;
using Cdk.Core.Common.Interface;

namespace Cdk.Core.FileManager
{
    public  static class FileManagerFactory
    {
        /// <summary>
        /// Factory method which encaosulates the creation of FileManager object
        /// TODo- Configure DI with IOC Container
        /// </summary>
        /// <param name="fileName">Path of the File</param>
        /// <returns>IFileManager instance for the file passed</returns>
        public static IFileManager GetFileManager(string fileName)
        {
            if (fileName == null) throw new ArgumentNullException("fileName");
            return new FileManagerRa(fileName);
        }
    }
}
