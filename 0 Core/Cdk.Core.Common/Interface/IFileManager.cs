﻿using System;
using System.IO;

namespace Cdk.Core.Common.Interface
{
    /// <summary>
    /// Encapsulates the File Management operations
    /// Implements IDisposable pattern
    /// </summary>
  public  interface IFileManager :IDisposable
    {
        FileStream GetFileStream();
        StreamReader GetStreamReader();
        void Initialize(int bufferSize = 1000);
        string GetLine(long position, bool resetBufferPostion);
        long ReadLineAndGetPosition(out string lineoftext);
        void ResetStream();
    }
}
