﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Dash = Cdk.Dashboard.Manger;
using Cdk.Dashboard.Manger;
using ConsoleApp;


namespace Cdk.Dashboard.ConsoleApp
{
    class Program
    {
        // Can be used for DI or with IOC container 
        static readonly IConsoleOperation ConsoleOperation = new DashboardConsoleOperation();
        static void Main(string[] args)
        {
            ConsoleOperation.Invoke();
        }
        
      
    }
}
