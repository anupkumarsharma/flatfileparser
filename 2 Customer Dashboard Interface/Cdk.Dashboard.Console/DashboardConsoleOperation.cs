﻿using System;
using System.Diagnostics;
using System.Linq;
using ConsoleApp;
using Dash = Cdk.Dashboard.Manger;

namespace Cdk.Dashboard.ConsoleApp
{
    /// <summary>
    /// Encapsulate the console operation for Dashboard operation
    /// </summary>
    public class DashboardConsoleOperation : IConsoleOperation
    {
        private static Dash.Dashboard _dashboardManager;
        private static string _exitCode;
        private static readonly Stopwatch StopWatch = new Stopwatch();

        public void Invoke()
        {
            Console.WriteLine(ConsoleResource.IndexBuildRequest);
            var isIndexBuildRequested = SanitizeAndGetInputFromConsole(true,new[]{"Y","y","N" ,"n"});
            if (isIndexBuildRequested == "Y")
            {
                StopWatch.Start();
                _dashboardManager = Dash.FactoryDashboard.GetDashBoardObject(true);
                StopWatch.Stop();
                Console.WriteLine(ConsoleResource.IndexBuildTime, StopWatch.ElapsedMilliseconds);
                StopWatch.Reset();

                SanitizeAndGetInputFromConsole(false, null);
            }

            else
            {
                _dashboardManager = Dash.FactoryDashboard.GetDashBoardObject(false);
            }
            do
            {
                Console.Clear();
                Console.WriteLine(ConsoleResource.CustomerIdRequest);
                try
                {
                    //Get customer Id from console as input 
                    int id = Int32.Parse(SanitizeAndGetInputFromConsole(true, null)); ;
                    //start the stopWatch 
                    StopWatch.Start();
                    //Get Customer Name from DashBoard

                    var customerName = _dashboardManager.GetcustomerName(id);

                    Console.WriteLine(ConsoleResource.CustomeName , customerName);

                    //Get List of orders
                    var orderList = _dashboardManager.GetOrdersForCustomer(id);
                    if (orderList != null)
                    {
                        Console.WriteLine(ConsoleResource.Ordersize, orderList.Count());
                        foreach (var order in orderList)
                        {

                            Console.WriteLine(ConsoleResource.OrderId, order.OrderId);
                            Console.WriteLine(ConsoleResource.CustomerId, order.CustomerId);
                            Console.WriteLine(ConsoleResource.CarID, order.NameOfCar);
                            Console.WriteLine(ConsoleResource.CarManufacturer, order.NameOfCarManufacturer);

                        }
                        StopWatch.Stop();
                        Console.WriteLine(ConsoleResource.QueryTime, StopWatch.ElapsedMilliseconds);
                        StopWatch.Reset();
                    }

                    else
                    {
                        Console.WriteLine(ConsoleResource._No_Order_FoundError);
                    }

                }
                catch (Dash.CustomerNotFoundException)
                {
                    Console.WriteLine(ConsoleResource._No_customer_found_with_this_Id);
                
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.ToString());
                    SanitizeAndGetInputFromConsole(false, null);
                    break;
                }
                Console.WriteLine(ConsoleResource.ContinuetoIterate);
                _exitCode = SanitizeAndGetInputFromConsole(false, null);
            }
            while (_exitCode =="13");


        }

          private static string SanitizeAndGetInputFromConsole(bool isLine, string[] acceptedCharacters)
          {
              if (isLine)
            {
                // Get the line from console 
                var consoleInput = Console.ReadLine().Trim();

                //check if the line is empty, this happens when user press enter, Inthis cases retry 
                while (String.IsNullOrEmpty(consoleInput))
               {
                    consoleInput = Console.ReadLine().Trim();
               }

               if (acceptedCharacters == null)
                   return consoleInput;

                //Sanitize the input by checking the acceptable value
               if (acceptedCharacters.Contains(consoleInput))
                   return consoleInput.Trim().ToUpper();
               else
               // retry endlessly for the correct value incase the value dont match
               {
                   string input;
                   do
                   {
                       Console.WriteLine();
                        input =Console.ReadLine().Trim();

                   } while (acceptedCharacters.Contains(input));

                   return (input.Trim().ToUpper());
               
               }
                   


            }
              return(Console.Read().ToString());
          }
    }
}
