﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cdk.Dashboard.ConsoleApp
{
    /// <summary>
    /// Defines the contract for any Console operation 
    /// </summary>
    interface IConsoleOperation
    {
        void Invoke();
    }
}
