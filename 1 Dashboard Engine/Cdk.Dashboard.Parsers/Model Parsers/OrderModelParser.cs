﻿using Cdk.Core.Common;
using Cdk.Dashboard.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cdk.Core.Common.Interface;

namespace Cdk.Dashboard.Parsers.Model_Parsers
{
    /// <summary>
    /// Encapsulated the Parsing logic for Cusomer Model 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class OrderModelParser<T> : IModelParser<T> where T : OrderModel
    {
        readonly IFileManager _fileManager;

         public OrderModelParser(IFileManager orderFileManager)
        {
            _fileManager = orderFileManager;
        }

         /// <summary>
         /// Locate the position of record in datasource and return the model list
         /// </summary>
         /// <param name="listofFilepointers"></param>
         /// <returns></returns>
        public  List<T> ParseIntoModelFromFile(List<long> listofFilepointers)
        {
            List<T> orderModelList = listofFilepointers.Select(pointer => ParseIntoModel(_fileManager.GetLine(pointer, true))).ToList();

          return (orderModelList);
        }

      /// <summary>
      /// convert the string into model of T
      /// </summary>
      /// <param name="recordString">T</param>
      /// <returns></returns>

        public T ParseIntoModel(string recordString)
        {
            string[] orderRecordArray = recordString.Split(new char[] { ',' });

            return (T) new OrderModel()
            {
                  OrderId = Int32.Parse(orderRecordArray[0]),
                  CustomerId = Int32.Parse(orderRecordArray[1]),
                  NameOfCar = orderRecordArray[2],
                 NameOfCarManufacturer= orderRecordArray[3],
                   ColorCar = orderRecordArray[4],
                 YearOfPurchase=  orderRecordArray[5],
                  CarType = (OrderModel.OwnerShipType)Enum.Parse(typeof(OrderModel.OwnerShipType), orderRecordArray[6])

            };
        }

       
    }
}
