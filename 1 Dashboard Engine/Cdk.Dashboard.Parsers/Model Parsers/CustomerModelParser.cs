﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cdk.Core.Common.Interface;
using Cdk.Dashboard.Model;

namespace Cdk.Dashboard.Parsers.Model_Parsers
{
    /// <summary>
    /// Encapsulated the Parsing logic for Cusomer Model 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CustomerModelParser<T> : IModelParser<T> where T:CustomerModel
    {
        readonly IFileManager _fileManager;

        public CustomerModelParser(IFileManager customerFileManager)
        {
            _fileManager = customerFileManager;
        }

        /// <summary>
        /// Locate the position of record in datasource and return the model list
        /// </summary>
        /// <param name="listofFilepointers"></param>
        /// <returns></returns>
        public List<T> ParseIntoModelFromFile(List<long> listofFilepointers)
        {
            List<T> customerModelList = listofFilepointers.Select(pointer => ParseIntoModel(_fileManager.GetLine(pointer, true))).ToList();

            return (customerModelList);
        }
        /// <summary>
        /// convert the string into model of T
        /// </summary>
        /// <param name="recordString">T</param>
        /// <returns></returns>
        public T ParseIntoModel(string recordString)
        {

            string[] customerRecordArray = recordString.Split(new char[] { ',' });

            return (T) new CustomerModel()
            {
                CustomerId = Int32.Parse(customerRecordArray[0]),
                FirstName = customerRecordArray[1],
                LastName = customerRecordArray[2],
                Address = customerRecordArray[3],
                Region = customerRecordArray[4],
                State = customerRecordArray[5],
                PinCode = customerRecordArray[6],

            };
        }
    }
}
