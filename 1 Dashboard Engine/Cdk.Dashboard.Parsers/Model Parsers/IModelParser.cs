﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cdk.Dashboard.Parsers.Model_Parsers
{

    /// <summary>
    /// this interface encapsulates the model parsing from text to model
    /// </summary>
   public interface IModelParser<T>
    {
        T ParseIntoModel(string s);
        List<T> ParseIntoModelFromFile(List<long> listofFilepointers);

    }
}
