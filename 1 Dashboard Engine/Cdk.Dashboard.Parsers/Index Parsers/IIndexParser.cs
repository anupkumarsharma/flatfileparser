﻿namespace Cdk.Dashboard.Parsers.Index_Parsers
{
   public interface IIndexParser<TKey,TValue>
    {
        bool Parse(out TKey key, out TValue value);
        void Reset();
        
    }
}
