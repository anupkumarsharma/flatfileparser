﻿using System;
using System.Linq;
using Cdk.Core.Common.Interface;
using Cdk.Dashboard.Model;

namespace Cdk.Dashboard.Parsers.Index_Parsers
{
    /// <summary>
    /// Encapsulated the Parsing logic for Customer Index
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CustomerIndexParser<T> : IIndexParser<int, T> where T : FilePointer
    {
        readonly IFileManager _fileManager;

        public CustomerIndexParser(IFileManager fileManager)
        {
            _fileManager = fileManager;
            _fileManager.Initialize(1000);

        }
        /// <summary>
        /// Parse for key and value from underlying datasource
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        /// <returns>false if datasource is completely parsed</returns>
        public bool Parse(out int key, out T value)
        {

            string record;

            var position = _fileManager.ReadLineAndGetPosition(out record);

            if (position == -1)
            {
                key = -1;
                value = null;
                return false;
            }
            value = (T)new FilePointer() { Filepointer = position };
            key = Int32.Parse(ExtractKeyFromCustomerRecord(record));

            return true;

        }


        /// <summary>
        /// Encapsulated the logic to parse for key
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        private string ExtractKeyFromCustomerRecord(string record)
        {
            return (record.Split(',').First());
        }



/// <summary>
/// Reset the datasource
/// </summary>

        public void Reset()
        {
            _fileManager.ResetStream();
        }
    }
}
