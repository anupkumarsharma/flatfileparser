﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Cdk.Dashboard.DataRepository;
using Cdk.Dashboard.Model;
using Cdk.Dashboard.Parsers;
using Cdk.Dashboard.Parsers.Model_Parsers;

namespace Cdk.Dashboard.Manger
{
    public class Dashboard 
    {

        //initialize data repository
        private readonly IRepository<CustomerModel> _customerRepository;
       private readonly IRepository<OrderModel> _orderRepository;
       private const string CustomerFileKeyName = "CustomerFilePath";
       private const string OrderFileKeyName = "OrderFilePath";
        

            public Dashboard (bool prebuildIndex)
	{
                //Read the file paths from app.config
                var customerFilePath = ConfigurationManager.AppSettings[CustomerFileKeyName];
        var orderFilePath = ConfigurationManager.AppSettings[OrderFileKeyName];
        if (String.IsNullOrEmpty(customerFilePath) || String.IsNullOrEmpty(orderFilePath))
        {
            throw new ArgumentNullException("prebuildIndex");
        
        }
        var repository = new RepositoryManager(prebuildIndex, customerFilePath, orderFilePath);
        _customerRepository = new CustomerRepository<CustomerModel>(repository, new CustomerModelParser<CustomerModel>(repository.GetDataManager(RepositoryRegistry.Customer)));
        _orderRepository = new OrderRepository<OrderModel>(repository,new OrderModelParser<OrderModel>(repository.GetDataManager(RepositoryRegistry.Order)));

                    
	}
        public string GetcustomerName(int id)
        {
            var customerModel = _customerRepository.GetById(id);
            if (customerModel != null)
            {
                return (_customerRepository.GetById(id).First().FirstName);
            }

            throw  new CustomerNotFoundException();
        }


        public List<OrderModel> GetOrdersForCustomer(int id)
        {
            var listOfModel = _orderRepository.GetById(id);
            if (listOfModel != null)
                return (listOfModel.ToList());
            else
                return (null);
            
        
        }

       
    }
}
