﻿using System;

namespace Cdk.Dashboard.Manger
{
    public class CustomerNotFoundException:Exception
    {
        public override string Message
        {
            get
            {
                return "Customer Not found";
            }
        }

    }
}
