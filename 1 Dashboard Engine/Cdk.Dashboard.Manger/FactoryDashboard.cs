﻿namespace Cdk.Dashboard.Manger
{
    public static class FactoryDashboard
    {
        /// <summary>
        /// Encapsulated the Dasboard object encapsulation
        /// </summary>
        /// <param name="prebuildIndex">trigger the index to build itself</param>
        /// <returns></returns>
        public static  Dashboard GetDashBoardObject(bool prebuildIndex)
        {
            if(prebuildIndex)
                 return(new Dashboard(true));
            return(new Dashboard(false));
        }
    }
}
