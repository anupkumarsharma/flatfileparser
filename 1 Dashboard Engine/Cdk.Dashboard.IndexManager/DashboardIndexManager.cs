﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cdk.Core.Common.Interface;
using Cdk.Dashboard.IndexManager.Index;
using Cdk.Dashboard.IndexManager.Indexer;
using Cdk.Dashboard.Model;

namespace Cdk.Dashboard.IndexManager
{
    /// <summary>
    /// Encapsulates the operation for Indexes used for this requirement
    /// </summary>
    public class DashboardIndexManager : IIndexManager
    {
        
        Dictionary<IndexType, IIndex<FilePointer>> _listOfIndexes;
        readonly IIndexer<FilePointer> _indexer;
        public DashboardIndexManager(IFileManager customerRepository, IFileManager orderRepository)
        {
            _indexer = new CustomerIndexer<FilePointer>(customerRepository, orderRepository);
            _listOfIndexes = _indexer.RegisterIndex();

        }
        /// <summary>
        /// Initiates the indexes to build itself
        /// </summary>
        public void PreBuildIndex()
        {
            _listOfIndexes = _indexer.BuildIndex();
        }

        /// <summary>
        /// Get the value for the a particular index
        /// </summary>
        /// <param name="key"></param>
        /// <param name="indexType"> Type of index to be used </param>
        /// <returns></returns>
  
        public  ICollection<long> GetIndexValue(int key, IndexType indexType)
        {
            ICollection<long> _value = null;
            bool isElementIndex = false;
            if ( _listOfIndexes == null )
            {
                throw new ArgumentNullException("_listOfIndexes");
            
            }
              //check if any index have entry - if yes then index is built for that value
              //else we will find the value and add it to index

            
              _listOfIndexes.ToList().ForEach((t)=>{

                  if (t.Value.ContainsKey(key))
                  {
                      isElementIndex = true;
                  
                  }
              });
                 
              // The element doesnt exist in any of the index and final attempt will be made to find 
            if (!isElementIndex)
            {
                BuildindexForValue(key, indexType);
            }
              
            _value = _listOfIndexes[indexType].GetValue(key).Select(t => t.Filepointer).ToList();

            return (_value);
        
        }

        /// <summary>
        /// Find and build the index for a particular indexType for a particular Id
        /// </summary>
        /// <param name="key"></param>
        /// <param name="indexType"></param>
  
        public void BuildindexForValue(int key, IndexType indexType)
          {
              _indexer.BuildindexForValue(key, _listOfIndexes);
          
          }



    }
}
