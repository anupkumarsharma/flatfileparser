﻿using System.Collections.Generic;
using Cdk.Dashboard.Parsers;
using Cdk.Dashboard.Parsers.Index_Parsers;

namespace Cdk.Dashboard.IndexManager.Index
{

    /// <summary>
    /// Encapsulates the Order Index operations
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    public class OrderIndex<T> : Index<int, List<T>>, IIndex<T> 
    {
        readonly IIndexParser<int, T> _parser;
        readonly int _countofIndexStopScan;
        public OrderIndex(IIndexParser<int, T> orderIndexParser, int stopScanOnKeyCount)
        {
            _countofIndexStopScan = stopScanOnKeyCount;
            _parser = orderIndexParser;
        }
        /// <summary>
        /// Add value to the index
        /// </summary>
        /// <param name="customerid">key</param>
        /// <param name="filePointerLocation">value</param>
        public void AddIndex(int customerid, T filePointerLocation)
        {
            Add(customerid, filePointerLocation);

        }

        
        /// <summary>
        /// This overrides the existing defination of Add by 
        /// adding multiple values for a given key
        /// </summary>
        /// <param name="key">key </param>
        /// <param name="value">value</param>
        /// <returns></returns>
        protected override bool Add(int key, List<T> value)
        {
            if (IsExist(key))
            {
                // the Id already exists so add to the list of value
                getValue(key).AddRange(value);
                return true;
            }
            
           base.Add(key, value);

            return true;
        }
        /// <summary>
        /// Add the key in index
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected  bool Add(int key, T value)
        {
            
            var newlist = new List<T>();
            newlist.Add(value);
            Add(key, newlist);
            return (true);

        }

        /// <summary>
        /// Remove the value from index
        /// </summary>
        /// <param name="customerid">key </param>
        public void RemoveIndex(int customerid)
        {
            Remove(customerid);
        }

        /// <summary>
        ///  This uses parser for parsing the underlying 
        /// data source to build the index. It builds the index for complete data 
        /// </summary>
        public void Build( )
        {
            bool endofRecord;
            do
            {
                T pointer;
                int key;
                endofRecord = _parser.Parse(out  key, out pointer);
                 AddIndex(key, pointer);

            } while (endofRecord);
        }

        /// <summary>
        /// Find the key, this uses parser for parsing the underlying 
        /// data source to build the index 
        /// </summary>
        /// <param name="id">key</param>
        public void Find(int id)
        {
            bool endofRecord;
            int count = 0;
            _parser.Reset();
            do
            {
                int key;
                T pointer;
                endofRecord = _parser.Parse(out  key, out pointer);
                if (key == id )
                {
                    AddIndex(key, pointer);
                    count++;
                    // check for any stopcount
                    if (_countofIndexStopScan == count)
                        endofRecord = false;
                }

            } while (endofRecord);

        }

        /// <summary>
        /// Get the value for aparticular key
        /// </summary>
        /// <param name="key">key</param>
        /// <returns></returns>
        public  ICollection<T> GetValue(int key)
        {
           return( getValue(key));
        }



        /// <summary>
        /// Check the key is already added to the index 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ContainsKey(int id)
        {
            return (IsExist(id));
        }
    }
}
