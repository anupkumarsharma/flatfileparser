﻿using System.Collections.Generic;

namespace Cdk.Dashboard.IndexManager.Index
{
    /// <summary>
    /// Interfaces for the index operations
    /// </summary>
    /// <typeparam name="T"></typeparam>
  public  interface IIndex<T>
    {
        void Build();
        void AddIndex(int customerid, T filePointerLocation);
        void RemoveIndex(int customerid);
        ICollection<T> GetValue(int key);
        void Find(int id);
        bool ContainsKey(int id);
    }


 
    
    
}
