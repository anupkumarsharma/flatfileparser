﻿using System.Collections.Generic;
using System.Linq;

namespace Cdk.Dashboard.IndexManager.Index
{
    /// <summary>
    /// base class for the index operation
    /// </summary>
    /// <typeparam name="T1">key</typeparam>
    /// <typeparam name="T2">value</typeparam>
    public  class Index<T1,T2>  
    {
        private  readonly Dictionary<T1, T2> _indexDictionary;

        public Index()
        {
            _indexDictionary = new Dictionary<T1, T2>();
        }

        protected virtual bool IsExist(T1 key)
        {
            return (_indexDictionary.Keys.Contains(key));
        }


        protected virtual bool Add(T1 key, T2 value)
        {
            if (_indexDictionary.Keys.Contains(key))
            {
                return false;
            }
            _indexDictionary.Add(key, value);
            return true;
                        
        }

        protected virtual bool Remove(T1 key)
        {
            if (!_indexDictionary.Keys.Contains(key))
            {
                return false;
            }
            _indexDictionary.Remove(key);
            return true;

        }

        protected virtual T2 getValue(T1 key)
        {
            
            return (_indexDictionary[key]);
        
        }


       
        

        
    }
}
