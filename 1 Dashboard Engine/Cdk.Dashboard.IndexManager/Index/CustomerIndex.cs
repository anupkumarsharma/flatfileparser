﻿using System.Collections.Generic;
using Cdk.Dashboard.Parsers;
using Cdk.Dashboard.Parsers.Index_Parsers;

namespace Cdk.Dashboard.IndexManager.Index
{
    /// <summary>
    /// Encapsulates the Customer Index operations
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    public class CustomerIndex<T> : Index<int, T>, IIndex<T> 
    {
        readonly int _countofIndexStopScan;
        readonly IIndexParser<int, T> _parser;
        public CustomerIndex(IIndexParser<int, T> customerIndexParser, int stopScanOnKeyCount)
        
        {
            _countofIndexStopScan = stopScanOnKeyCount;
            _parser = customerIndexParser;
        }
        /// <summary>
        /// Add value to the index
        /// </summary>
        /// <param name="customerid">key</param>
        /// <param name="filePointerLocation">value</param>
        public void AddIndex(int customerid, T filePointerLocation)
        {
            Add(customerid, filePointerLocation);
        
        }

        /// <summary>
        /// Remove the value from index
        /// </summary>
        /// <param name="customerid">key </param>
        public void RemoveIndex(int customerid)
        {
            Remove(customerid);

        }

        /// <summary>
        /// Find the key, this uses parser for parsing the underlying 
        /// data source to build the index 
        /// </summary>
        /// <param name="id">key</param>
        public void Find(int id)
        {
            bool endofRecord;
            int count =0;
            _parser.Reset();
            do
            {
                T pointer;
                int key;
                endofRecord = _parser.Parse(out  key, out pointer);
                if (key == id)
                {
                    AddIndex(key, pointer); 
                    count++;
                        // check for any stopcount
                    if (_countofIndexStopScan == count)
                            endofRecord = false;
                }

            } while (endofRecord);
        
        }

        /// <summary>
        ///  This uses parser for parsing the underlying 
        /// data source to build the index. It builds the index for complete data 
        /// </summary>
        public  void  Build()
        {
            bool endofRecord;
            do
            {
                T pointer;
                int key;
                endofRecord = _parser.Parse(out  key, out pointer);
                if(key!=-1)
                AddIndex(key, pointer);

            } while (endofRecord);
        }

        /// <summary>
        /// Get the value for aparticular key
        /// </summary>
        /// <param name="key">key</param>
        /// <returns></returns>
        public  ICollection<T> GetValue(int key)
        {
            return( new T[]{base.getValue(key)});
        }

        /// <summary>
        /// Check the key is already added to the index 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ContainsKey(int id)
        {
            return IsExist(id);
        }
    }
}
