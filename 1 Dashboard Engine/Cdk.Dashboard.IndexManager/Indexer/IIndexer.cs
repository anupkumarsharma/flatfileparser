﻿
using System.Collections.Generic;
using Cdk.Dashboard.IndexManager.Index;

namespace Cdk.Dashboard.IndexManager.Indexer
{
    public interface IIndexer<T>
    {
        Dictionary<IndexType, IIndex<T>> BuildIndex();
        Dictionary<IndexType, IIndex<T>> RegisterIndex();
        void BuildindexForValue(int key, Dictionary<IndexType, IIndex<T>> indexlist);
    }
}
