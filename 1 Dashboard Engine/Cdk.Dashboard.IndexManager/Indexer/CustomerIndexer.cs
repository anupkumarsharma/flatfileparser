﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using Cdk.Core.Common.Interface;
using Cdk.Dashboard.IndexManager.Index;
using Cdk.Dashboard.Model;
using Cdk.Dashboard.Parsers;
using Cdk.Dashboard.Parsers.Index_Parsers;

namespace Cdk.Dashboard.IndexManager.Indexer
{
    /// <summary>
    /// Encapsulates the index base operation of any index 
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    public class CustomerIndexer<T> : IIndexer<T> where T:FilePointer
    {
        readonly IFileManager _customerRepository;
        readonly IFileManager _orderRepository;
        private const string Indexsectionname = "IndexManagerSection";
        private const string Customerindexname = "CustomerRecordMaxOccurence";
        private const string Orderindexname = "OrderRecordMaxOccurence";
        private  int _customerScanCount;
        private  int _orderScanCount;
        public Dictionary<IndexType, IIndex<T>> Indexlist;
        public CustomerIndexer(IFileManager customerRepository, IFileManager orderRepository)
        {
           
            ParseConfiguration();
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
            Indexlist = new Dictionary<IndexType, IIndex<T>>();
            RegisterIndex();
            

        }

        /// <summary>
        /// This method will parse all configuration.
        /// Throw ArgumentNullException for any configuration value not found
        /// </summary>
        private void ParseConfiguration()
        {
            var sectionKeys = ConfigurationManager.GetSection(Indexsectionname) as NameValueCollection;
            if (sectionKeys == null)
            {
                throw new ArgumentNullException( "Index Section Not Found");
            }
            var cindex=sectionKeys[Customerindexname];
            var oindex = sectionKeys[Orderindexname];
            if (String.IsNullOrEmpty(cindex) || String.IsNullOrEmpty(oindex))
            { 
                throw new ArgumentNullException("Unable to locate the configuration Values");
            }
            _customerScanCount = Int32.Parse(cindex);
            _orderScanCount = Int32.Parse(oindex);


        }
        // RegisterAll Indexes
        
        /// <summary>
        /// Register all the indexes availbale
        /// TODO - To be used in conjuction with IOC containter 
        /// </summary>
        /// <returns></returns>
        public Dictionary<IndexType, IIndex<T>> RegisterIndex()
      {
            Indexlist = new Dictionary<IndexType, IIndex<T>>
            {
                {IndexType.Customer, new CustomerIndex<T>(new CustomerIndexParser<T>(_customerRepository), 1)},
                {IndexType.Order, new OrderIndex<T>(new OrderIndexParser<T>(_orderRepository), 10)}
            };
            return (Indexlist);
      
      }

        /// <summary>
        /// Build all the indexes which is registered 
        /// </summary>
        /// <returns></returns>
      public Dictionary<IndexType, IIndex<T>> BuildIndex()
        {
            foreach (var item in Indexlist)
            {
                item.Value.Build();
                
            }

            return (Indexlist);
        }

        /// <summary>
        /// Build Index for a particular value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="indexlist"></param>
      public void BuildindexForValue(int key, Dictionary<IndexType, IIndex<T>> indexlist)
      {
          foreach (var item in indexlist)
          {
              item.Value.Find(key);

          }

          


      }

    }
}
