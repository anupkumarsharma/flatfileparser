﻿namespace Cdk.Dashboard.IndexManager
{
    /// <summary>
    /// Enumeration of all registered index
    /// </summary>
    public enum IndexType
    {
        Customer,
        Order
    }
}
