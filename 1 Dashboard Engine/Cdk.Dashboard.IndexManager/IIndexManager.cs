﻿using System.Collections.Generic;

namespace Cdk.Dashboard.IndexManager
{
   public interface IIndexManager
    {
         ICollection<long> GetIndexValue(int key, IndexType indexType);
            void PreBuildIndex();
            void BuildindexForValue(int key, IndexType indexType);
    }
}
