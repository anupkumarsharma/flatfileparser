﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cdk.Dashboard.Model
{


    /// <summary>
    /// Order Model
    /// </summary>
  
    public class OrderModel
    {
        public enum OwnerShipType
        {
            Own,
            Lease
        }

        public int OrderId { get; set; }
        public int CustomerId { get; set; }
        public string NameOfCarManufacturer { get; set; }
        public string NameOfCar { get; set; }
        public string ColorCar { get; set; }
        public string YearOfPurchase { get; set; }
        public OwnerShipType CarType;
        


    }
}
