﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cdk.Dashboard.Model
{
    /// <summary>
    /// Customer Model
    /// </summary>
    public class CustomerModel
    {

        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Region { get; set; }
        public string State { get; set; }
        public string PinCode { get; set; }

    }
}
