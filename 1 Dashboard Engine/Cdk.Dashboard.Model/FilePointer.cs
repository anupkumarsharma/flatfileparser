﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cdk.Dashboard.Model
{
 /// <summary>
 /// object which stores the pointer in a particlar file
 /// </summary>
    public class FilePointer
    {
      public  long Filepointer { get; set; }
    }
}
