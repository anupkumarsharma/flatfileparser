﻿using Cdk.Core.Common;
using Cdk.Core.FileManager;
using Cdk.Dashboard.IndexManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cdk.Core.Common.Interface;

namespace Cdk.Dashboard.DataRepository
{
    /// <summary>
    /// This acts as Repository Base Class which encapsulates the index management
    /// and maintains the  index for a pariticular  value.This lets a record to be added 
    /// to the index for a particular Id or key of the index 
    /// </summary>
    public class RepositoryManager : IRepositoryManager
    {


        public DashboardIndexManager IndexManager;
        public IFileManager CustomerFileManager;
        public IFileManager OrderFileManager;

        public RepositoryManager(bool preBuildIndex, string customerFileName, string orderFileName)
        {
            CustomerFileManager = FileManagerFactory.GetFileManager(customerFileName);
            OrderFileManager = FileManagerFactory.GetFileManager(orderFileName);
            IndexManager = new DashboardIndexManager(CustomerFileManager, OrderFileManager);
            //build index 
            if (preBuildIndex)
            {
                IndexManager.PreBuildIndex();
            }

        }

        public IIndexManager GetIndexer()
        {
            return IndexManager;
        
        }


        public void InitializeRepository(IFileManager orderFileManagerInstance)
        {
            this.OrderFileManager = orderFileManagerInstance;
        }

       public void GetCustomerRecord(int customerId)
        {
            IndexManager.GetIndexValue(customerId, IndexType.Customer);
        }

       public void GetOrderRecord(int customerId)
        {
            IndexManager.GetIndexValue(customerId, IndexType.Order);
        }


        public void Dispose()
        {
            OrderFileManager.Dispose();
            CustomerFileManager.Dispose();
        }


        public IFileManager GetDataManager(RepositoryRegistry repositoryType)
        {
            switch (repositoryType)
            {
                case RepositoryRegistry.Order:
                    return (OrderFileManager);
                    
                case RepositoryRegistry.Customer:
                    return (CustomerFileManager);
                    
                default:
                    throw new ArgumentOutOfRangeException("repositoryType");
                    
            }
        }
    }
}
