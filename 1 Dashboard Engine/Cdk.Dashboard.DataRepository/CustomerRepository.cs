﻿using Cdk.Dashboard.IndexManager;
using Cdk.Dashboard.Model;
using Cdk.Dashboard.Parsers;
using Cdk.Dashboard.Parsers.Model_Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cdk.Dashboard.DataRepository
{
    /// <summary>
    /// Encapsulates index and operation for the customer file
    /// </summary>
    /// <typeparam name="T">Customer Model</typeparam>
   public class CustomerRepository<T> : IRepository<T> where T:CustomerModel
    {
       readonly IRepositoryManager _repositoryManager;
       readonly IModelParser<CustomerModel> _parser;

       public CustomerRepository(IRepositoryManager repositoryManager,  IModelParser<CustomerModel> cusomerParser)
       {
           _repositoryManager = repositoryManager;
           _parser = cusomerParser;

       }
       /// <summary>
       /// Get the Model/collection of models for a given Id
       /// </summary>
       /// <param name="id">Customer Id</param>
       /// <returns>Collection of the Customer Model</returns>
       /// <exception cref="KeyNotFoundException "></exception>
        public ICollection<T> GetById(int id) 
        {
            ICollection<long> listofrecordPointers;
            try
            {
                 listofrecordPointers = _repositoryManager.GetIndexer().GetIndexValue(id, IndexType.Customer);
            }

            catch (KeyNotFoundException Ex)
            {
                listofrecordPointers = null;
            
            }
          if (listofrecordPointers != null)
          {
              var listOfModel = _parser.ParseIntoModelFromFile(listofrecordPointers.ToList());
              return ((ICollection<T>)listOfModel);
          }
           return (null);
        }

       /// <summary>
       /// Dispose the underlying repository 
       /// </summary>
        public void Dispose()
        {
            _repositoryManager.Dispose();
        }
    }
}
