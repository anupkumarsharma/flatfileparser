﻿using System;
using System.Collections.Generic;

namespace Cdk.Dashboard.DataRepository
{
    /// <summary>
    /// Encapsulates the operation which entity can perform
    /// </summary>
    /// <typeparam name="T"> Type T</typeparam>
    public interface IRepository<T>:IDisposable where T:class
    {
        ICollection<T> GetById(int id);
    }
}
