﻿using System.Collections.Generic;
using System.Linq;
using Cdk.Dashboard.IndexManager;
using Cdk.Dashboard.Model;
using Cdk.Dashboard.Parsers.Model_Parsers;

namespace Cdk.Dashboard.DataRepository
{
    /// <summary>
    /// Encapsulated the respository for Order Entity
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class OrderRepository<T> : IRepository<T> where T : OrderModel
    {
        readonly IRepositoryManager _repositoryManager;
        readonly IModelParser<OrderModel> _parser;

        public OrderRepository(IRepositoryManager repositoryManager,IModelParser<OrderModel> orderModelParser )
        {

            _repositoryManager = repositoryManager;
            _parser = orderModelParser;

        }
        public ICollection<T> GetById(int id)
        {

            ICollection<long> listofrecordPointers;
            try
            {
                listofrecordPointers = _repositoryManager.GetIndexer().GetIndexValue(id, IndexType.Order);
            }

            catch (KeyNotFoundException)
            {
                listofrecordPointers = null;

            }
            if (listofrecordPointers != null)
            {
                var listOfModel = _parser.ParseIntoModelFromFile(listofrecordPointers.ToList());
                return ((ICollection<T>)listOfModel);
            }

            return (null);


        }

        public void Dispose()
        {
            _repositoryManager.Dispose();
        }
    }
}
