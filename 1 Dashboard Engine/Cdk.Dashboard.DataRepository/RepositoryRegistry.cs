﻿namespace Cdk.Dashboard.DataRepository
{
    /// <summary>
    /// maintains the list Entity maintained 
    /// by the Repository layer 
    /// </summary>
   public enum RepositoryRegistry
    {
        Order,
        Customer
    }
}
