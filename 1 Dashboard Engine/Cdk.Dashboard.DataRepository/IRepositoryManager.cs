﻿using System;
using Cdk.Core.Common.Interface;
using Cdk.Dashboard.IndexManager;

namespace Cdk.Dashboard.DataRepository
{
   /// <summary>
   /// Encapsulates the Respository manager operation
   /// consolidated base class for any underlying operation 
   /// of entity 
   /// </summary>
    public interface IRepositoryManager : IDisposable
    {
        /// <summary>
        /// Get the Customer Record 
        /// </summary>
        /// <param name="customerId">Customer ID</param>
 
        void GetCustomerRecord(int customerId);
        /// <summary>
        /// Get the Order Record
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        void GetOrderRecord(int customerId);
        /// <summary>
        /// Get the underlying indexer
        /// </summary>
        /// <returns></returns>
         IIndexManager GetIndexer();
        /// <summary>
        /// Get the DataManager/Actual data handle being used 
        /// </summary>
        /// <param name="repositoryType"></param>
        /// <returns></returns>
         IFileManager GetDataManager(RepositoryRegistry repositoryType);
    }
}
